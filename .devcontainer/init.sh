pip install -r requirements.txt

echo 'alias run="uvicorn main:app --reload"' >> /home/vscode/.zshrc;
echo 'alias makemigrations="alembic revision --autogenerate -m"' >> /home/vscode/.zshrc;
echo 'alias migrate="alembic upgrade head"' >> /home/vscode/.zshrc;
echo 'alias test="coverage run -m pytest && coverage report"' >> /home/vscode/.zshrc;
echo 'alias lint = "flake8"' >> /home/vscode/.zshrc;
