pip3 install -r requirements.txt

echo 'alias run="uvicorn main:app --reload"' >> /Users/cihatbaran/.zshrc;
echo 'alias makemigrations="alembic revision --autogenerate -m"' >> /Users/cihatbaran/.zshrc;
echo 'alias migrate="alembic upgrade head"' >> /Users/cihatbaran/.zshrc;
echo 'alias test="coverage run -m pytest && coverage report"' >> /Users/cihatbaran/.zshrc;
echo 'alias lint = "flake8"' >> /Users/cihatbaran/.zshrc;
