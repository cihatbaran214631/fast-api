import os

import uvicorn
from dotenv import load_dotenv
from fastapi import FastAPI, Depends
from fastapi_sqlalchemy.middleware import DBSessionMiddleware


from app.api.api_v1 import api
from app.api.deps import get_superuser
from app.db.database import SQLALCHEMY_DATABASE_URL

# # base dir settings
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
load_dotenv(os.path.join(BASE_DIR, ".env"))

description = """
- Gives summary and examples of FastApi documentation
- <a href="https://gitlab.com/cihatbaran214631/fast-api/-/tree/main" target="_blank"> Visit Project </a>
"""

# fast api
app = FastAPI(title="Tutorial FastApi...",
              description=description,
              docs_url="/")
app.add_middleware(DBSessionMiddleware, db_url=SQLALCHEMY_DATABASE_URL)

app.include_router(api.router)


@app.get("/api/v1/test_detail", dependencies=[Depends(get_superuser)])
def list_detail():
    return "this endpoint works just great..."


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True)
