from app.core.config import settings

SQLALCHEMY_DATABASE_URL = f"postgresql+psycopg2://postgres:{settings.PASSWORD}@{settings.HOST}:{settings.PORT}"