from typing import Optional, Any

from sqlalchemy.orm import Session

from app.core.security.hash import get_password_hashed, verify_password
from app.crud.base import CRUDBase
from app.models.user import User
from app.schemas.user import CreateUser
from fastapi.encoders import jsonable_encoder


class CRUDUser(CRUDBase):

    def create(self, db: Session, obj_in: CreateUser) -> User:
        hashed_pass = get_password_hashed(obj_in.password)
        obj_in.password = hashed_pass
        return super().create(db, obj_in)

    def list(self, db: Session) -> Optional[User]:
        return super().get_all(db)

    def authenticate(self, db: Session, *, username: str, password: str) -> Optional[User]:
        d_user = self.get_by_username(db, username=username)
        if not d_user:
            return None
        if not verify_password(password, d_user.password):
            return None
        return d_user

    @staticmethod
    def get_by_username(db: Session, username: str) -> Optional[User]:
        return db.query(User).filter(User.username == username).first()

    @staticmethod
    def is_superuser(c_user: User) -> bool:
        return c_user.super_user


crud_user = CRUDUser(User)
