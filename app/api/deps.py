from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from jose import jwt

from app.core.config import settings
from app.core.dependencies import get_session
from app.core.security.oauth import oauth2_scheme
from app.crud.crud_user import crud_user
from app.models.user import User
from app.schemas.token import TokenPayload


def get_superuser(
    db: Session = Depends(get_session), token: str = Depends(oauth2_scheme)
) -> User:
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
        del payload['exp']
        token_data = TokenPayload(**payload)

        user = crud_user.get_by_username(db, token_data.sub)
    except Exception:
        raise HTTPException(status_code=400, detail="Incorrect jwt token")

    if not user.super_user:
        raise HTTPException(status_code=400, detail="You are not super user!")
    return user

