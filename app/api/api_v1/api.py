from fastapi import APIRouter

from app.api.api_v1.endpoints import ping, tutorial, users
from app.core.config import settings
from app.core.dependencies import oauth2_dependency
from app.core.security import oauth

router = APIRouter()

router.include_router(ping.router,
                      tags=["ping"],
                      prefix=settings.API_V1_STR)
router.include_router(tutorial.router,
                      tags=["tutorial"],
                      dependencies=[oauth2_dependency],
                      prefix=settings.API_V1_STR)
router.include_router(users.router, tags=["users"],
                      prefix=settings.API_V1_STR,
                      dependencies=[oauth2_dependency])
router.include_router(oauth.router)
