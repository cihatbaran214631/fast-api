import time

from fastapi import APIRouter, Response, Depends, Request
from fastapi import BackgroundTasks

router = APIRouter(prefix="/ping")


def ping():
    print("sleeping")
    time.sleep(5)


@router.post("/send-notification/")
async def send_notification(background_tasks: BackgroundTasks):

    background_tasks.add_task(ping)
    return {"message": "Notification sent in the background"}


@router.post("/cookie-and-object/")
def create_cookie(response: Response):

    response.set_cookie(key="fakesession", value="fake-cookie-session-value")

    return {"message": "Come to the dark side, we have cookies"}


class FixedContentQueryChecker:

    def __init__(self, fixed_content: str):

        self.fixed_content = fixed_content

    def __call__(self, q: str = ""):
        if q:
            return self.fixed_content in q
        return False


checker = FixedContentQueryChecker("bar")


@router.get("/query-checker/")
async def read_query_check(*, fixed_content_included: bool = Depends(checker), request: Request):
    return {"fixed_content_in_query": fixed_content_included}
