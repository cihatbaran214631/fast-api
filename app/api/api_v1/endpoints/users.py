from typing import List

from fastapi import (APIRouter,
                     Depends)
from fastapi_sqlalchemy import db
from sqlalchemy.orm import Session

from app.api.deps import get_superuser
from app.core.dependencies import get_session
from app.crud.crud_user import crud_user
from app.schemas.user import ListUser, CreateUser

router = APIRouter()


@router.post("/user/", status_code=201, response_model=CreateUser, dependencies=[Depends(get_superuser)])
async def create_users(user: CreateUser, dp_db: Session = Depends(get_session)):
    db_user = crud_user.create(dp_db, user)
    return db_user


@router.get("/users/", status_code=200, response_model=List[ListUser],
            response_model_exclude_unset=True)
async def get_users():
    all_users = crud_user.list(db.session)
    return all_users
