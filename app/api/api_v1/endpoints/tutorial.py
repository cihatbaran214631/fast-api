from typing import (List,
                    Optional,
                    Set,
                    Union)
from uuid import UUID
from fastapi import (APIRouter,
                     Body,
                     status,
                     Depends)
from fastapi.exceptions import HTTPException
from pydantic import (BaseModel,
                      Field,
                      validator)

router = APIRouter()


class Item(BaseModel):
    name: str
    description: Optional[str] = Field(None,
                                       title="the description of the item")
    price: float
    tax: Optional[float] = None


class User(BaseModel):
    id: int
    uuid: UUID
    name: str
    array: List[dict] = list()
    set_val: Set[str] = set()
    item: List[Item]
    password: str

    @validator('password')
    def validate_password(cls, v):
        if len(v) < 3:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f"{v} password is not safe",
                                headers={"detail": "custom error header"})
        return v


class ResponseUser(User):
    pass


class ResponseSuperUser(User):
    super_user: bool = False

    class Config:
        title = "Super users from database"


class RequestUser(User):
    password: str

    class Config:
        title = "Third party users"


class CommonQueryParams:
    def __init__(self, q: str, skip: int = 0, limit: int = Body(100)):
        self.q = q
        self.skip = skip
        self.limit = limit


@router.post("/test/", status_code=201, description="This is description <br> - this is value",
             response_model=Union[ResponseSuperUser, ResponseUser], response_model_exclude={"password"},
             response_description="The created item")
async def create_item(user: User = Body(...), commons: CommonQueryParams = Depends()):
    return user


@router.get("/newspaper/", summary="It is not being used anymore just demonstration purpose",
            openapi_extra={"x-aperture-labs-portal": "blue"})
async def get_newspaper():
    return "ok"
