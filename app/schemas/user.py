from pydantic import BaseModel


class SchemeUser(BaseModel):
    first_name: str
    last_name: str
    username: str
    age: int

    class Config:
        orm_mode = True


class CreateUser(SchemeUser):
    password: str
    super_user: bool = False

    class Config:
        orm_mode = True


class ListUser(SchemeUser):
    class Config:
        orm_mode = True
