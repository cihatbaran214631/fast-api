import os
from pydantic import BaseSettings


class Settings(BaseSettings):
    API_V1_STR: str = "/api/v1"
    # jwt
    SECRET_KEY: str = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
    ALGORITHM: str = "HS256"
    ACCESS_TOKEN_EXPIRE_SECONDS: int = 60 * 60 * 2

    # db
    HOST: str = os.getenv('DB_HOST', '127.0.0.1')
    PORT: int = os.getenv('DB_PORT', 5432)
    USER: str = os.getenv('DB_USER', 'postgres')
    PASSWORD: str = os.getenv('DB_PASSWORD', 'postgres')
    NAME: str = os.getenv('DB_NAME', 'idnet')


settings = Settings()
