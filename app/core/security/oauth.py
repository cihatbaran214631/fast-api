from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi import Depends, APIRouter
from fastapi import HTTPException
from fastapi_sqlalchemy import db
from jose import jwt

from app.core.config import settings
from app.core.security.hash import verify_password
from app.core.security.token_val import Token, create_access_token
from app.crud.crud_user import crud_user
from app.models.user import User as ModelUser
from app.schemas.token import TokenPayload

router = APIRouter(tags=["authentication"])


@router.post("/token", response_model=Token)
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    try:
        user_data = db.session.query(ModelUser).\
            filter_by(username=form_data.username).one()
    except Exception:
        raise HTTPException(status_code=400, detail="Incorrect username")

    matched_password = verify_password(form_data.password, user_data.password)

    if not matched_password:
        raise HTTPException(status_code=400, detail="Incorrect password")

    # user_dict = {column.name: str(getattr(user_data, column.name)) for column in user_data.__table__.columns}
    user_dict = {"sub": user_data.username}

    access_token = create_access_token(
        data=user_dict
    )

    return {"access_token": access_token, "token_type": "bearer"}

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


async def validate_token(token: str = Depends(oauth2_scheme)):
    try:
        payload = jwt.decode(token,
                             settings.SECRET_KEY,
                             algorithms=[settings.ALGORITHM])
        del payload['exp']
        token_data = TokenPayload(**payload)

        crud_user.get_by_username(db.session, token_data.sub)
    except Exception:
        raise HTTPException(status_code=400, detail="Incorrect jwt token")
    return True
