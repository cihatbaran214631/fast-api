from datetime import datetime, timedelta
from typing import Optional
from jose import jwt
from pydantic import BaseModel

from app.core.config import settings


class Token(BaseModel):
    access_token: str
    token_type: str


def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode,
                             settings.SECRET_KEY,
                             algorithm=settings.ALGORITHM)
    return encoded_jwt


# print(create_access_token({"username": "cihat", "password": "cihat", "email": "cihatbarann@gmail.com"}))
