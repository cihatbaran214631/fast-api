from sqlalchemy.orm import Session
from sqlalchemy import create_engine
from fastapi import Depends

from app.core.security import oauth
from app.db.database import SQLALCHEMY_DATABASE_URL


def get_session():  # noqa
    engine = create_engine(SQLALCHEMY_DATABASE_URL)
    with Session(engine) as session:
        yield session


oauth2_dependency = Depends(oauth.validate_token)
